﻿using System.Web.Http;

namespace AngularMVC_WebAPI.Controllers
{
    [RoutePrefix("Contact")]
    public class ContactController : ApiController
    {
        public IHttpActionResult Get()
        {
            var contact = new ContactDto()
            {
                Name = "Toan Phan"
            };

            return Ok(contact);
        }

        [Route("Get/{contactNumber}")]
        public IHttpActionResult Get(int contactNumber)
        {
            var contact = new ContactDto()
            {
                Name = "Toan Phan"
            };

            if(contactNumber == 1)
                return Ok(contact);

            if (contactNumber == 2)
                return BadRequest();

            else
                return null;
        }
    }

    public class ContactDto
    {
        public string Name { get; set; }
    }
}
