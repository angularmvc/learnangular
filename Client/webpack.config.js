﻿const webpack = require("webpack");
const path = require("path");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const entryPath = path.resolve(__dirname, "modules");
const corePath = path.resolve(
  __dirname,
  "../Server/AngularMVC/AngularMVC_WebAPI/Scripts/ng2"
);
const viewPath = path.resolve(
    __dirname,
    "../Server/AngularMVC/AngularMVC_WebAPI/Views/Home"
  );
  const mainModule = `${entryPath}/main-app`;

module.exports = envOptions => {
  envOptions = envOptions || {};

  console.log("test part");
  var rootDir = __dirname.split('\\');
  rootDir = rootDir.slice(0, rootDir.length-1);
  rootDir = rootDir.join('\\');
  console.log(rootDir); 

  const config = {
    entry: {
      polyfills: `${entryPath}/polyfill.ts`,
      vendors: `${entryPath}/vendor.ts`,
      mainModule: `${mainModule}/main.ts` 
    },
    output: {
      path: corePath,
      filename: "[name].[hash].js",
      sourceMapFilename: "[name].[hash].js.map"
    },
    resolve: {
      extensions: [".ts", ".js", ".html"]
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          loaders: ["awesome-typescript-loader", "angular2-template-loader"]
        },
        {
          test: /\.html$/,
          loader: "raw-loader"
        },
        {
          test: /\.css$/,
          loader: "raw-loader"
        }
      ]
    },
    devtool: "source-map",
    plugins: [
      new webpack.NoEmitOnErrorsPlugin(),
      new webpack.optimize.CommonsChunkPlugin({
        name: ["vendors", "polyfills"]
      }),
      new HtmlWebpackPlugin({
        template: viewPath + "/loader.cshtml",
        filename: viewPath + "/Index.cshtml",
        inject: false
      })
    ]
  };

  if (envOptions.MODE === "prod") {
    config.plugins.push(
      new UglifyJsPlugin()      
    );
  }

  return config;
};
