import { Component } from "@angular/core";

@Component({
  selector: "main-app",
  templateUrl: "./main.html"
})
export class AppComponent {
  description = "Integrate Angular + Webpack into a ASP.NET MVC 5 application";
}
