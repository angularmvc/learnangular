import { Component, Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { ApiService } from "../../services/api.service";
import { ContactUrls } from "./urls";

@Injectable()
export class ContactService {
    constructor(private _api: ApiService) {
    }
    
    getContact(contactNumber: number) {
        return this._api.get(
            ContactUrls.getContact.replace("{contactNumber}", contactNumber.toString())
        );
    }
}
