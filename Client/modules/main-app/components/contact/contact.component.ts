import { Component } from "@angular/core";
import { ContactService } from "./contact.service";

@Component({
  selector: "contact",
  templateUrl: "./contact.component.html"
})

export class ContactComponent {
    constructor(private contactService: ContactService) {

    }

    getContact(contactNumber: number) {
        return this.contactService.getContact(contactNumber);
    }
}
