import { Component, Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Urls } from "../urls";
import { HelperService } from "./helper.service";

@Injectable()
export class ApiService {
    constructor(private http: HttpClient,
        private helperService: HelperService
    ) {}

    private baseUrl = Urls.prototype.BaseUrl;
    
    getContact() {
        this.http.get(this.baseUrl + "/api/contact")
            .subscribe(result => {
                console.log(result);
            });
    }

    get<R>(url: string, params?: any) {
        return this.http.get(url, {params: params, observe: 'response'})
        .subscribe(
            data => {
                console.log(data);
                return data;
            },
            error => {
                console.log(error);
            }
        );
    }
}
