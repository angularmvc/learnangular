import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';
import { ContactService } from './components/contact/contact.service';
import { ContactComponent } from './components/contact/contact.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './services/api.service';
import { HelperService } from './services/helper.service';

@NgModule({
  imports:      [ BrowserModule, HttpClientModule ],
  declarations: [ AppComponent, ContactComponent ],
  bootstrap:    [ AppComponent ],
  providers:    [ ApiService, ContactService, HelperService ]
})
export class AppModule { }
